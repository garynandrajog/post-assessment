package com.xnsio.cleancode;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

import static org.junit.Assert.assertEquals;

public class MeetingAssistantTest {

    private Schedule firstParticipantSchedule;
    private Schedule secondParticipantSchedule;
    private Schedule thirdParticipantSchedule;
    private Schedule fourthParticipantSchedule;
    private Schedule fifthParticipantSchedule;
    private Schedule cutOffDate1;
    private Schedule cutOffDate2;
    private Schedule cutOffDate3;
    private Schedule freeTimeSlot;
    private ArrayList<Schedule> listOfSchedules;
    private int generatedFreeSlotTime;
    private String generatedFreeSlotDate;


    @Before
    public void initializeSchedules() {
        TreeMap<LocalDate, ArrayList<Integer>> schedule1 = new TreeMap<>();
        TreeMap<LocalDate, ArrayList<Integer>> schedule2 = new TreeMap<>();
        TreeMap<LocalDate, ArrayList<Integer>> schedule3 = new TreeMap<>();
        TreeMap<LocalDate, ArrayList<Integer>> schedule4 = new TreeMap<>();
        TreeMap<LocalDate, ArrayList<Integer>> schedule5 = new TreeMap<>();

        schedule1.put(LocalDate.now(), new ArrayList<>(Arrays.asList(8, 9, 11, 14, 16)));
        schedule2.put(LocalDate.now(), new ArrayList<>(Arrays.asList(8, 10, 12)));
        schedule3.put(LocalDate.now(), new ArrayList<>(Arrays.asList(8, 9, 12, 14)));
        schedule4.put(LocalDate.now(), new ArrayList<>(Arrays.asList(8, 9, 11, 13, 14, 16)));
        schedule5.put(LocalDate.now(), new ArrayList<>(Arrays.asList(10, 12, 13, 15)));

        schedule1.put(LocalDate.now().plusDays(1), new ArrayList<>(Arrays.asList()));
        schedule2.put(LocalDate.now().plusDays(1), new ArrayList<>(Arrays.asList()));
        schedule3.put(LocalDate.now().plusDays(1), new ArrayList<>(Arrays.asList()));
        schedule4.put(LocalDate.now().plusDays(1), new ArrayList<>(Arrays.asList()));
        schedule5.put(LocalDate.now().plusDays(1), new ArrayList<>(Arrays.asList()));

        firstParticipantSchedule = new Schedule(schedule1);
        secondParticipantSchedule = new Schedule(schedule2);
        thirdParticipantSchedule = new Schedule(schedule3);
        fourthParticipantSchedule = new Schedule(schedule4);
        fifthParticipantSchedule = new Schedule(schedule5);

        cutOffDate1 = new Schedule(LocalDate.now(), 15);
        cutOffDate2 = new Schedule(LocalDate.now(), 9);
        cutOffDate3 = new Schedule(LocalDate.now().plusDays(1), 12);
    }


    @Test
    public void getNoFreeSlots() {
        generateListOfSchedules(fourthParticipantSchedule, fifthParticipantSchedule);

        when().passListOfSchedulesToAssistant(cutOffDate1).itReturnsNoFreeSlot();
    }

    @Test
    public void getFreeSlotsForNextDay() {
        generateListOfSchedules(fourthParticipantSchedule, fifthParticipantSchedule);

        when().passListOfSchedulesToAssistant(cutOffDate3).itReturnsFreeSlot(LocalDate.now().plusDays(1).toString(), 8);
    }

    @Test
    public void getFreeSlotsForFourParticipants() {
        generateListOfSchedules(firstParticipantSchedule, secondParticipantSchedule, thirdParticipantSchedule, fourthParticipantSchedule);

        when().passListOfSchedulesToAssistant(cutOffDate1).itReturnsFreeSlot(LocalDate.now().toString(), 15);
    }

    @Test
    public void getNoFreeSlotsForTwoParticipantsBeforeCutOff() {
        generateListOfSchedules(firstParticipantSchedule, secondParticipantSchedule);

        when().passListOfSchedulesToAssistant(cutOffDate2).itReturnsNoFreeSlot();
    }


    private MeetingAssistantTest when() {
        return this;
    }

    private MeetingAssistantTest passListOfSchedulesToAssistant(Schedule cutOffDate) {
        freeTimeSlot = MeetingAssistant.getFreeTimeSlotFor(listOfSchedules, cutOffDate);
        return this;
    }

    private void generateListOfSchedules(Schedule... schedule) {
        listOfSchedules = new ArrayList<>();

        for (Schedule individualSchedules : schedule) {
            listOfSchedules.add(individualSchedules);
        }
    }

    private void itReturnsFreeSlot(String date, int freeSlot) {
        validateFreeSlot(date, freeSlot);
    }

    private void itReturnsNoFreeSlot() {
        validateFreeSlot(null, 0);
    }

    private void validateFreeSlot(String expectedDate, int expectedTimeSlot) {
        extractFreeSlot();
        assertFreeSlot(expectedDate, expectedTimeSlot);
    }

    private void extractFreeSlot() {
        Map.Entry<LocalDate, Integer> schedule = freeTimeSlot.getScheduleEntry();
        LocalDate date = schedule.getKey();
        if (date == null) {
            generatedFreeSlotTime = 0;
            generatedFreeSlotDate = null;
        } else {
            generatedFreeSlotTime = schedule.getValue();
            generatedFreeSlotDate = date.toString();
        }
    }

    private void assertFreeSlot(String expectedDate, int expectedTimeSlot) {
        assertEquals(generatedFreeSlotDate, expectedDate);
        assertEquals(generatedFreeSlotTime, expectedTimeSlot);
    }
}