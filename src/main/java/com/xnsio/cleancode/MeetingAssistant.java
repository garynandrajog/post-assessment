package com.xnsio.cleancode;

import java.time.LocalDate;
import java.util.*;

class MeetingAssistant {

    private static ArrayList<Integer> officeHours = new ArrayList<>(Arrays.asList(8, 9, 10, 11, 12, 13, 14, 15, 16));

    private MeetingAssistant() {
    }

    static Schedule getFreeTimeSlotFor(List<Schedule> schedules, Schedule cutOffDate) {
        int i = 0;
        for (Schedule individualSchedule : schedules) {
            schedules.set(i, getFreeTimeSlotFor(individualSchedule));
            i++;
        }

        return getCommonSlot(schedules, cutOffDate);
    }

    private static Schedule getFreeTimeSlotFor(Schedule schedule) {
        return convertScheduleToFreeHoursFromBusy(schedule);
    }

    private static Schedule convertScheduleToFreeHoursFromBusy(Schedule schedule) {
        for (Map.Entry<LocalDate, ArrayList<Integer>> dayWiseSchedule : schedule.getSchedule().entrySet()) {
            ArrayList<Integer> daySlots = new ArrayList<>(officeHours);
            ArrayList<Integer> busyHours = dayWiseSchedule.getValue();
            daySlots.removeAll(busyHours);
            schedule.getSchedule().put(dayWiseSchedule.getKey(), daySlots);
        }

        return schedule;
    }

    private static Schedule getCommonSlot(List<Schedule> schedules, Schedule cutOffDateTime) {

        LocalDate cutOffDate = cutOffDateTime.getScheduleEntry().getKey();
        int cutOffTime = cutOffDateTime.getScheduleEntry().getValue();
        LocalDate finalDate = null;
        int timeSlot = 0;

        for (LocalDate date : schedules.get(0).getSchedule().keySet()) {
            ArrayList<ArrayList<Integer>> timeSlots = new ArrayList<>();
            for (ListIterator<Schedule> iter = schedules.listIterator(0); iter.hasNext(); ) {
                timeSlots.add(iter.next().getSchedule().get(date));
            }

            ArrayList<Integer> commonFreeSlots = new ArrayList<>(timeSlots.get(0));
            for (ListIterator<ArrayList<Integer>> iter = timeSlots.listIterator(0); iter.hasNext(); ) {
                commonFreeSlots.retainAll(iter.next());
            }

            if (!commonFreeSlots.isEmpty()) {
                finalDate = date;
                timeSlot = commonFreeSlots.get(0);
            }

            if (cutOffDate.isBefore(date)) {
                finalDate = null;
                timeSlot = 0;
                break;
            }

            if (cutOffDate.isEqual(date)) {
                if (cutOffTime < timeSlot) {
                    finalDate = null;
                    timeSlot = 0;
                    break;
                }
            }

            if(finalDate!=null && timeSlot!=0){
                break;
            }
        }

        return new Schedule(finalDate, timeSlot);
    }
}