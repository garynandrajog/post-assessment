package com.xnsio.cleancode;

import java.time.LocalDate;
import java.util.*;

class Schedule {
    private TreeMap<LocalDate, ArrayList<Integer>> scheduleMap;
    private Map.Entry<LocalDate, Integer> scheduleEntry;

    Schedule(TreeMap<LocalDate, ArrayList<Integer>> schedule) {
        this.scheduleMap = schedule;
    }

    Schedule(LocalDate date, int timeSlot) {
        this.scheduleEntry = new AbstractMap.SimpleEntry<>(date, timeSlot);
    }

    TreeMap<LocalDate, ArrayList<Integer>> getSchedule() {
        return scheduleMap;
    }

    Map.Entry<LocalDate, Integer> getScheduleEntry() {
        return scheduleEntry;
    }
}